package com.hsd.rssreader.provider;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;

import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.Channel;
import com.hsd.rssreader.data.bean.FeedItem;
import com.hsd.rssreader.utils.PrimitiveUtils;

public class DataProviderFacade {

	public static Cursor getChannelsCursor(Context context) {
		return context.getContentResolver().query(Consts.CONTENT_CHANNEL_URI, Consts.PROJECTION_CHANNELS, null, null, null);
	}

	public static List<Channel> getChannels(Context context) {
		List<Channel> channels = new LinkedList<Channel>();
		Cursor channelsCursor = getChannelsCursor(context);
		if (channelsCursor.getCount() > 0) {
			while (channelsCursor.moveToNext()) {
				Integer id = channelsCursor.getInt(Consts.COLUMN_INDEX_ID);
				String title = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_TITLE);
				String userTitle = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_USER_TITLE);
				String link = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_LINK);
				String linkMD5 = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_LINK_MD5);
				Date lastBuildDate = new Date(channelsCursor.getInt(Consts.COLUMN_INDEX_CHANNEL_LAST_BUILD_DATE));
				Date downloadDate = new Date(channelsCursor.getInt(Consts.COLUMN_INDEX_CHANNEL_DOWNLOAD_DATE));
				channels.add(new Channel(id, title, userTitle, link, linkMD5, lastBuildDate, downloadDate));
			}
			channelsCursor.close();
		} else {
			channelsCursor.close();
		}
		return channels;
	}

	public static Channel getChannelById(Context context, Integer channelId) {
		Cursor channelsCursor = context.getContentResolver().query(Consts.CONTENT_CHANNEL_URI, Consts.PROJECTION_CHANNELS, Consts.DB_COLUMN_ID + "=?", new String[] {"" + channelId}, null);;
		if (channelsCursor.getCount() > 0) {
			channelsCursor.moveToNext();
			Integer id = channelsCursor.getInt(Consts.COLUMN_INDEX_ID);
			String title = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_TITLE);
			String userTitle = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_USER_TITLE);
			String link = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_LINK);
			String linkMD5 = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_LINK_MD5);
			Date lastBuildDate = new Date(channelsCursor.getInt(Consts.COLUMN_INDEX_CHANNEL_LAST_BUILD_DATE));
			Date downloadDate = new Date(channelsCursor.getInt(Consts.COLUMN_INDEX_CHANNEL_DOWNLOAD_DATE));
			Channel channel = new Channel(id, title, userTitle, link, linkMD5, lastBuildDate, downloadDate);
			channelsCursor.close();
			return channel;
		} else {
			channelsCursor.close();
		}
		return null;
	}

	public static Channel getChannelByLink(Context context, String link) {
		Cursor channelsCursor = context.getContentResolver().query(Consts.CONTENT_CHANNEL_URI, Consts.PROJECTION_CHANNELS, Consts.DB_COLUMN_CHANNEL_LINK + "=?", new String[] {link}, null);;
		if (channelsCursor.getCount() > 0) {
			channelsCursor.moveToNext();
			Integer id = channelsCursor.getInt(Consts.COLUMN_INDEX_ID);
			String title = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_TITLE);
			String userTitle = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_USER_TITLE);
			String linkMD5 = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_LINK_MD5);
			Date lastBuildDate = new Date(channelsCursor.getInt(Consts.COLUMN_INDEX_CHANNEL_LAST_BUILD_DATE));
			Date downloadDate = new Date(channelsCursor.getInt(Consts.COLUMN_INDEX_CHANNEL_DOWNLOAD_DATE));
			Channel channel = new Channel(id, title, userTitle, link, linkMD5, lastBuildDate, downloadDate);
			channelsCursor.close();
			return channel;
		} else {
			channelsCursor.close();
		}
		return null;
	}

	public static Cursor getFeeditemsCursor(Context context) {
		return context.getContentResolver().query(Consts.CONTENT_FEEDITEM_URI, Consts.PROJECTION_FEEDITEMS, null, null, Consts.DB_COLUMN_FEEDITEM_PUB_DATE + " DESC");
	}

	public static List<FeedItem> getFeeditems(Context context) {
		List<FeedItem> feeditems = new LinkedList<FeedItem>();
		Cursor feeditemsCursor = getFeeditemsCursor(context);
		if (feeditemsCursor.getCount() > 0) {
			while (feeditemsCursor.moveToNext()) {
				Integer id = feeditemsCursor.getInt(Consts.COLUMN_INDEX_ID);
				Integer channelId = feeditemsCursor.getInt(Consts.COLUMN_INDEX_FEEDITEM_CHANNEL_ID);
				String title = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_TITLE);
				String link = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_LINK);
				String linkMD5 = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_LINK_MD5);
				String description = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_DESCRIPTION);
				Date pubDate = new Date(feeditemsCursor.getLong(Consts.COLUMN_INDEX_FEEDITEM_PUB_DATE));
				Boolean isRead = feeditemsCursor.getInt(Consts.COLUMN_INDEX_FEEDITEM_IS_READ) == 1;
				Boolean toSendEmail = feeditemsCursor.getInt(Consts.COLUMN_INDEX_FEEDITEM_TO_SEND_EMAIL) == 1;
				feeditems.add(new FeedItem(id, channelId, title, link, linkMD5, description, pubDate, isRead, toSendEmail));
			}
			feeditemsCursor.close();
		} else {
			feeditemsCursor.close();
		}
		return feeditems;
	}

	public static Cursor getChannelItemsCursor(Context context, Integer channelId) {
		return context.getContentResolver().query(Consts.CONTENT_FEEDITEM_URI, Consts.PROJECTION_FEEDITEMS, Consts.DB_COLUMN_FEEDITEM_CHANNEL_ID + "=?", new String[] { "" + channelId }, Consts.DB_COLUMN_FEEDITEM_PUB_DATE + " DESC");
	}

	public static List<FeedItem> getChannelItems(Context context, Integer channelId) {
		List<FeedItem> feeditems = new LinkedList<FeedItem>();
		Cursor feeditemsCursor = getChannelItemsCursor(context, channelId);
		if (feeditemsCursor.getCount() > 0) {
			while (feeditemsCursor.moveToNext()) {
				Integer id = feeditemsCursor.getInt(Consts.COLUMN_INDEX_ID);
				String title = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_TITLE);
				String link = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_LINK);
				String linkMD5 = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_LINK_MD5);
				String description = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_DESCRIPTION);
				Date pubDate = new Date(feeditemsCursor.getLong(Consts.COLUMN_INDEX_FEEDITEM_PUB_DATE));
				Boolean isRead = feeditemsCursor.getInt(Consts.COLUMN_INDEX_FEEDITEM_IS_READ) == 1;
				Boolean toSendEmail = feeditemsCursor.getInt(Consts.COLUMN_INDEX_FEEDITEM_TO_SEND_EMAIL) == 1;
				feeditems.add(new FeedItem(id, channelId, title, link, linkMD5, description, pubDate, isRead, toSendEmail));
			}
			feeditemsCursor.close();
		} else {
			feeditemsCursor.close();
		}
		return feeditems;
	}

	public static Cursor getMarkedChannelItemsCursor(Context context) {
		return context.getContentResolver().query(Consts.CONTENT_FEEDITEM_URI, Consts.PROJECTION_FEEDITEMS, Consts.DB_COLUMN_FEEDITEM_TO_SEND_EMAIL + "=?", new String[] { "1" }, Consts.DB_COLUMN_FEEDITEM_CHANNEL_ID + " ASC, " + Consts.DB_COLUMN_FEEDITEM_PUB_DATE + " DESC");
	}

	public static List<FeedItem> getChannelItemsForEmail(Context context) {
		List<FeedItem> feeditems = new LinkedList<FeedItem>();
		Cursor feeditemsCursor = getMarkedChannelItemsCursor(context);
		if (feeditemsCursor.getCount() > 0) {
			while (feeditemsCursor.moveToNext()) {
				Integer id = feeditemsCursor.getInt(Consts.COLUMN_INDEX_ID);
				Integer channelId = feeditemsCursor.getInt(Consts.COLUMN_INDEX_FEEDITEM_CHANNEL_ID);
				String title = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_TITLE);
				String link = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_LINK);
				String linkMD5 = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_LINK_MD5);
				String description = feeditemsCursor.getString(Consts.COLUMN_INDEX_FEEDITEM_DESCRIPTION);
				Date pubDate = new Date(feeditemsCursor.getLong(Consts.COLUMN_INDEX_FEEDITEM_PUB_DATE));
				Boolean isRead = feeditemsCursor.getInt(Consts.COLUMN_INDEX_FEEDITEM_IS_READ) == 1;
				Boolean toSendEmail = feeditemsCursor.getInt(Consts.COLUMN_INDEX_FEEDITEM_TO_SEND_EMAIL) == 1;
				feeditems.add(new FeedItem(id, channelId, title, link, linkMD5, description, pubDate, isRead, toSendEmail));
			}
			feeditemsCursor.close();
		} else {
			feeditemsCursor.close();
		}
		return feeditems;
	}

	// ********************* channel ********************

	public static void addChannel(Context context, String title, String userTitle, String link) {
		ContentValues values = new ContentValues();
		values.put(Consts.DB_COLUMN_CHANNEL_TITLE, title);
		values.put(Consts.DB_COLUMN_CHANNEL_USER_TITLE, userTitle);
		values.put(Consts.DB_COLUMN_CHANNEL_LINK, link);
		values.put(Consts.DB_COLUMN_CHANNEL_LINK_MD5, PrimitiveUtils.md5(link));
		context.getContentResolver().insert(Consts.CONTENT_CHANNEL_URI, values);
	}

	public static void deleteChannel(Context context, Channel channel) {
		context.getContentResolver().delete(Consts.CONTENT_CHANNEL_URI, Consts.DB_COLUMN_ID + "=?", new String[] { "" + channel.getId() });
	}

	public static void updateChannelTimestamp(Context context, Integer id, Date downloadDate) {
		ContentValues values = new ContentValues();
		values.put(Consts.DB_COLUMN_CHANNEL_DOWNLOAD_DATE, downloadDate.getTime());
		context.getContentResolver().update(Consts.CONTENT_CHANNEL_URI, values, Consts.DB_COLUMN_ID + "=?", new String[] { "" + id });
	}

	// ********************* feeditem ********************

	public static void addFeeditems(Context context, List<FeedItem> feeditems) {
		ContentValues[] listValues = new ContentValues[feeditems.size()];
		for (int i = 0; i < feeditems.size(); i++) {
			FeedItem feeditem = feeditems.get(i);
			ContentValues values = new ContentValues();
			values.put(Consts.DB_COLUMN_FEEDITEM_CHANNEL_ID, feeditem.getChannelId());
			values.put(Consts.DB_COLUMN_FEEDITEM_TITLE, feeditem.getTitle());
			values.put(Consts.DB_COLUMN_FEEDITEM_LINK, feeditem.getLink());
			values.put(Consts.DB_COLUMN_FEEDITEM_LINK_MD5, PrimitiveUtils.md5(feeditem.getLink()));
			values.put(Consts.DB_COLUMN_FEEDITEM_DESCRIPTION, feeditem.getDescription());
			values.put(Consts.DB_COLUMN_FEEDITEM_PUB_DATE, feeditem.getPubDate().getTime());
			listValues[i] = values;
		}
		try {
			context.getContentResolver().bulkInsert(Consts.CONTENT_FEEDITEM_URI, listValues);
		} catch (SQLException ce) {
			for (FeedItem feeditem : feeditems) {
				try {
					addFeeditem(context, feeditem.getChannelId(), feeditem.getTitle(), feeditem.getLink(), feeditem.getDescription(), feeditem.getPubDate());
				} catch (Exception ex) {
				}
			}
		}
	}

	public static void addFeeditem(Context context, Integer channelId, String title, String link, String description, Date pubDate) {
		ContentValues values = new ContentValues();
		values.put(Consts.DB_COLUMN_FEEDITEM_CHANNEL_ID, channelId);
		values.put(Consts.DB_COLUMN_FEEDITEM_TITLE, title);
		values.put(Consts.DB_COLUMN_FEEDITEM_LINK, link);
		values.put(Consts.DB_COLUMN_FEEDITEM_LINK_MD5, PrimitiveUtils.md5(link));
		values.put(Consts.DB_COLUMN_FEEDITEM_DESCRIPTION, description);
		values.put(Consts.DB_COLUMN_FEEDITEM_PUB_DATE, pubDate.getTime());
		context.getContentResolver().insert(Consts.CONTENT_FEEDITEM_URI, values);
	}

	public static void deleteFeeditem(Context context, FeedItem feeditem) {
		context.getContentResolver().delete(Consts.CONTENT_FEEDITEM_URI, Consts.DB_COLUMN_ID + "=?", new String[] { "" + feeditem.getId() });
	}

	public static void deleteOldFeeditems(Context context, Long timestamp) {
		context.getContentResolver().delete(Consts.CONTENT_FEEDITEM_URI, Consts.DB_COLUMN_FEEDITEM_PUB_DATE + "<?", new String[] { "" + timestamp });
	}

	public static void updateMarkReadFeeditem(Context context, FeedItem feeditem, Boolean isRead, Boolean toSendEmail) {
		ContentValues values = new ContentValues();
		if (isRead != null) {
			values.put(Consts.DB_COLUMN_FEEDITEM_IS_READ, isRead ? 1 : 0);
		}
		if (toSendEmail != null) {
			values.put(Consts.DB_COLUMN_FEEDITEM_TO_SEND_EMAIL, toSendEmail ? 1 : 0);
		}
		context.getContentResolver().update(Consts.CONTENT_FEEDITEM_URI, values, Consts.DB_COLUMN_ID + "=?", new String[] { "" + feeditem.getId() });
	}

	public static void updateMarkReadAllFeeditem(Context context) {
		ContentValues values = new ContentValues();
		values.put(Consts.DB_COLUMN_FEEDITEM_IS_READ, 1);
		context.getContentResolver().update(Consts.CONTENT_FEEDITEM_URI, values, null, null);
	}

	public static void updateMarkSentAllFeeditem(Context context) {
		ContentValues values = new ContentValues();
		values.put(Consts.DB_COLUMN_FEEDITEM_TO_SEND_EMAIL, 0);
		context.getContentResolver().update(Consts.CONTENT_FEEDITEM_URI, values, null, null);
	}

}
