package com.hsd.rssreader.provider;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import com.hsd.rssreader.consts.Consts;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class DataProvider extends ContentProvider {

    private static final String DATABASE_NAME = "rssreader.db";
    private static final int DATABASE_VERSION = 1;
	private static final String TABLE_NAME_CHANNEL = "CHANNEL";
	private static final String TABLE_NAME_FEEDITEM = "FEEDITEM";

	private static final String INDEX_NAME_CHANNEL_UNIQUE = "CHANNEL_UNIQUE";
	private static final String INDEX_NAME_FEEDITEM_UNIQUE = "FEEDITEM_UNIQUE";
 
    private static final UriMatcher sUriMatcher; 
    
    public static LinkedHashMap<String, String> sChannelProjectionMap;  
    public static LinkedHashMap<String, String> sFeeditemProjectionMap;  
    
    private static final int QUERY_CHANNEL = 1;
    private static final int QUERY_FEEDITEM = 2;
    
    /**
     * This class helps open, create, and upgrade the database file.
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {
    	private Context localContext;
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION); 
        	Log.w("DatabaseHelper", "DatabaseHelper(context)");
        	localContext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        	Log.w("DatabaseHelper", "onCreate()");
            db.execSQL("CREATE TABLE " + TABLE_NAME_CHANNEL + " ("
                    + Consts.DB_COLUMN_ID + " INTEGER PRIMARY KEY," 
            		+ Consts.DB_COLUMN_CHANNEL_TITLE + " TEXT not null,"
            		+ Consts.DB_COLUMN_CHANNEL_USER_TITLE + " TEXT not null,"
            		+ Consts.DB_COLUMN_CHANNEL_LINK + " TEXT not null,"
            		+ Consts.DB_COLUMN_CHANNEL_LINK_MD5 + " TEXT not null,"
            		+ Consts.DB_COLUMN_CHANNEL_LAST_BUILD_DATE + " INTEGER,"
            		+ Consts.DB_COLUMN_CHANNEL_DOWNLOAD_DATE + " INTEGER"
                    + ");");
            db.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS  " + INDEX_NAME_CHANNEL_UNIQUE + " on "
                    + TABLE_NAME_CHANNEL + " (" + Consts.DB_COLUMN_CHANNEL_LINK_MD5 + ");");
            db.execSQL("CREATE TABLE " + TABLE_NAME_FEEDITEM + " ("
                    + Consts.DB_COLUMN_ID + " INTEGER PRIMARY KEY,"
                    + Consts.DB_COLUMN_FEEDITEM_CHANNEL_ID + " INTEGER,"
            		+ Consts.DB_COLUMN_FEEDITEM_TITLE + " TEXT not null,"
            		+ Consts.DB_COLUMN_FEEDITEM_LINK + " TEXT not null,"
            		+ Consts.DB_COLUMN_FEEDITEM_LINK_MD5 + " TEXT not null,"
            		+ Consts.DB_COLUMN_FEEDITEM_DESCRIPTION + " INTEGER not null,"
            		+ Consts.DB_COLUMN_FEEDITEM_PUB_DATE + " INTEGER not null," 
            		+ Consts.DB_COLUMN_FEEDITEM_IS_READ + " INTEGER not null default false," 
            		+ Consts.DB_COLUMN_FEEDITEM_TO_SEND_EMAIL + " INTEGER not null default false,"
            		+ " FOREIGN KEY(" + Consts.DB_COLUMN_FEEDITEM_CHANNEL_ID + ") REFERENCES " + TABLE_NAME_CHANNEL + "(" + Consts.DB_COLUMN_ID + ") "
                    + ");");
            db.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS  " + INDEX_NAME_FEEDITEM_UNIQUE + " on "
                    + TABLE_NAME_FEEDITEM + " (" + Consts.DB_COLUMN_FEEDITEM_LINK_MD5 + ");");
            db.execSQL("INSERT INTO " + TABLE_NAME_CHANNEL + " ("
            		+ Consts.DB_COLUMN_CHANNEL_TITLE + ","
            		+ Consts.DB_COLUMN_CHANNEL_USER_TITLE + ","
            		+ Consts.DB_COLUMN_CHANNEL_LINK + ","
            		+ Consts.DB_COLUMN_CHANNEL_LINK_MD5
                    + ") VALUES ('Top stories', 'My top stories', 'http://rss.cnn.com/rss/edition.rss', '1');");
            db.execSQL("INSERT INTO " + TABLE_NAME_CHANNEL + " (" 
            		+ Consts.DB_COLUMN_CHANNEL_TITLE + ","
            		+ Consts.DB_COLUMN_CHANNEL_USER_TITLE + ","
            		+ Consts.DB_COLUMN_CHANNEL_LINK + ","
            		+ Consts.DB_COLUMN_CHANNEL_LINK_MD5
                    + ") VALUES ('World', 'My World', 'http://rss.cnn.com/rss/edition_world.rss', '2');");
            db.execSQL("INSERT INTO " + TABLE_NAME_CHANNEL + " ("
            		+ Consts.DB_COLUMN_CHANNEL_TITLE + ","
            		+ Consts.DB_COLUMN_CHANNEL_USER_TITLE + ","
            		+ Consts.DB_COLUMN_CHANNEL_LINK + ","
            		+ Consts.DB_COLUMN_CHANNEL_LINK_MD5
                    + ") VALUES ('Africa', 'My Africa', 'http://rss.cnn.com/rss/edition_africa.rss', '3');");
        }

        @Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        	Log.w("DatabaseHelper", "onUpgrade()");
			Log.w("onUpgrade", "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
			if (oldVersion < 200) { 
				// TODO : add code here
			}
		}
    }

    private DatabaseHelper mOpenHelper;

    @Override
    public boolean onCreate() {
    	Log.w("DataProvider", "onCreate()");
        mOpenHelper = new DatabaseHelper(getContext());
        return true;
    } 
    
    public int delete(Uri uri, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        switch (sUriMatcher.match(uri)) {
        case QUERY_CHANNEL:
            count = db.delete(TABLE_NAME_CHANNEL, where, whereArgs);
            break;
        case QUERY_FEEDITEM:
            count = db.delete(TABLE_NAME_FEEDITEM, where, whereArgs);
            break;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count; 
	}

	public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
        case QUERY_CHANNEL:
            return Consts.CONTENT_TYPE_CHANNEL;
        case QUERY_FEEDITEM:
            return Consts.CONTENT_TYPE_FEEDITEM;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        } 
    }

	public Uri insert(Uri uri, ContentValues initialValues) {
		SQLiteDatabase db;
		long rowId;
        switch (sUriMatcher.match(uri)) {
        case QUERY_CHANNEL:
            db = mOpenHelper.getWritableDatabase();
            rowId = db.insert(TABLE_NAME_CHANNEL, null, initialValues);
            if (rowId > 0) {
                Uri noteUri = ContentUris.withAppendedId(Consts.CONTENT_CHANNEL_URI, rowId);
                getContext().getContentResolver().notifyChange(noteUri, null);
                return noteUri;
            };
            break;
        case QUERY_FEEDITEM:
            db = mOpenHelper.getWritableDatabase();
            rowId = db.insert(TABLE_NAME_FEEDITEM, null, initialValues);
            if (rowId > 0) {
                Uri noteUri = ContentUris.withAppendedId(Consts.CONTENT_FEEDITEM_URI, rowId);
                getContext().getContentResolver().notifyChange(noteUri, null);
                return noteUri;
            };
            break;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        } 
        throw new SQLException("Failed to insert row into " + uri);
	}

	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch (sUriMatcher.match(uri)) {
        case QUERY_CHANNEL:
            qb.setTables(TABLE_NAME_CHANNEL);
            qb.setProjectionMap(sChannelProjectionMap);
        	break;
        case QUERY_FEEDITEM:
            qb.setTables(TABLE_NAME_FEEDITEM);
            qb.setProjectionMap(sFeeditemProjectionMap);
        	break;
        default:
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        // Get the database and run the query
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);

        // Tell the cursor what uri to watch, so it knows when its source data changes
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c; 
    }

	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		SQLiteDatabase db = mOpenHelper.getWritableDatabase();
		int count = 0;
        switch (sUriMatcher.match(uri)) {
        case QUERY_CHANNEL:
        	count = db.update(TABLE_NAME_CHANNEL, values, selection, selectionArgs);
        	break;
        case QUERY_FEEDITEM:
        	count = db.update(TABLE_NAME_FEEDITEM, values, selection, selectionArgs);
        	break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(Consts.AUTHORITY, TABLE_NAME_CHANNEL, QUERY_CHANNEL); 
        sUriMatcher.addURI(Consts.AUTHORITY, TABLE_NAME_FEEDITEM, QUERY_FEEDITEM); 

        sChannelProjectionMap = new LinkedHashMap<String, String>();
        sChannelProjectionMap.put(Consts.DB_COLUMN_ID, Consts.DB_COLUMN_ID); 
        sChannelProjectionMap.put(Consts.DB_COLUMN_CHANNEL_TITLE, Consts.DB_COLUMN_CHANNEL_TITLE); 
        sChannelProjectionMap.put(Consts.DB_COLUMN_CHANNEL_USER_TITLE, Consts.DB_COLUMN_CHANNEL_USER_TITLE); 
        sChannelProjectionMap.put(Consts.DB_COLUMN_CHANNEL_LINK, Consts.DB_COLUMN_CHANNEL_LINK); 
        sChannelProjectionMap.put(Consts.DB_COLUMN_CHANNEL_LINK_MD5, Consts.DB_COLUMN_CHANNEL_LINK_MD5); 
        sChannelProjectionMap.put(Consts.DB_COLUMN_CHANNEL_LAST_BUILD_DATE, Consts.DB_COLUMN_CHANNEL_LAST_BUILD_DATE); 
        sChannelProjectionMap.put(Consts.DB_COLUMN_CHANNEL_DOWNLOAD_DATE, Consts.DB_COLUMN_CHANNEL_DOWNLOAD_DATE);
        sFeeditemProjectionMap = new LinkedHashMap<String, String>();
        sFeeditemProjectionMap.put(Consts.DB_COLUMN_ID, Consts.DB_COLUMN_ID);
        sFeeditemProjectionMap.put(Consts.DB_COLUMN_FEEDITEM_CHANNEL_ID, Consts.DB_COLUMN_FEEDITEM_CHANNEL_ID);
        sFeeditemProjectionMap.put(Consts.DB_COLUMN_FEEDITEM_TITLE, Consts.DB_COLUMN_FEEDITEM_TITLE); 
        sFeeditemProjectionMap.put(Consts.DB_COLUMN_FEEDITEM_LINK, Consts.DB_COLUMN_FEEDITEM_LINK); 
        sFeeditemProjectionMap.put(Consts.DB_COLUMN_FEEDITEM_LINK_MD5, Consts.DB_COLUMN_FEEDITEM_LINK_MD5); 
        sFeeditemProjectionMap.put(Consts.DB_COLUMN_FEEDITEM_DESCRIPTION, Consts.DB_COLUMN_FEEDITEM_DESCRIPTION); 
        sFeeditemProjectionMap.put(Consts.DB_COLUMN_FEEDITEM_PUB_DATE, Consts.DB_COLUMN_FEEDITEM_PUB_DATE); 
        sFeeditemProjectionMap.put(Consts.DB_COLUMN_FEEDITEM_IS_READ, Consts.DB_COLUMN_FEEDITEM_IS_READ); 
        sFeeditemProjectionMap.put(Consts.DB_COLUMN_FEEDITEM_TO_SEND_EMAIL, Consts.DB_COLUMN_FEEDITEM_TO_SEND_EMAIL); 
    }
    
}
