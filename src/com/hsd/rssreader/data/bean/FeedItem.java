package com.hsd.rssreader.data.bean;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedItem implements Parcelable {

	private Integer id;
	private Integer channelId;
	private String title;
	private String link;
	private String linkMD5;
	private String description;
	private Date pubDate;
	
	private Boolean isRead;
	private Boolean toSendEmail;

	public FeedItem() {
		super();
	}
	public FeedItem(Integer id, Integer channelId, String title, String link, String linkMD5, String description, Date pubDate, Boolean isRead, Boolean toSendEmail) {
		super();
		this.id = id;
		this.channelId = channelId;
		this.title = title;
		this.link = link;
		this.linkMD5 = linkMD5;
		this.description = description;
		this.pubDate = pubDate;
		this.isRead = isRead;
		this.toSendEmail = toSendEmail;
	}
	public FeedItem(Integer channelId, String title, String link, String linkMD5, String description, Date pubDate) {
		super();
		this.channelId = channelId;
		this.title = title;
		this.link = link;
		this.linkMD5 = linkMD5;
		this.description = description;
		this.pubDate = pubDate;
	}
	private FeedItem(Parcel in) {
		id = in.readInt();
		channelId = in.readInt();
		title = in.readString();
		link = in.readString();
		linkMD5 = in.readString();
		description = in.readString();
		pubDate = new Date(in.readLong());
		isRead = in.readInt() == 1;
		toSendEmail = in.readInt() == 1;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getChannelId() {
		return channelId;
	}
	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getLinkMD5() {
		return linkMD5;
	}
	public void setLinkMD5(String linkMD5) {
		this.linkMD5 = linkMD5;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getPubDate() {
		return pubDate;
	}
	public void setPubDate(Date pubDate) {
		this.pubDate = pubDate;
	}
	public Boolean getIsRead() {
		return isRead;
	}
	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}
	public Boolean getToSendEmail() {
		return toSendEmail;
	}
	public void setToSendEmail(Boolean toSendEmail) {
		this.toSendEmail = toSendEmail;
	}
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeInt(channelId);
		dest.writeString(title);
		dest.writeString(link);
		dest.writeString(linkMD5);
		dest.writeString(description);
		dest.writeLong(pubDate.getTime());
		dest.writeInt(isRead ? 1 : 0);
		dest.writeInt(toSendEmail ? 1 : 0);
	}
	public static final Parcelable.Creator<FeedItem> CREATOR = new Parcelable.Creator<FeedItem>() {
		@Override
		public FeedItem createFromParcel(Parcel source) {
			return new FeedItem(source);
		}
		@Override
		public FeedItem[] newArray(int size) {
			return new FeedItem[size];
		}
	};
	@Override
	public String toString() {
		return "FeedItem [id=" + id + ", channelId=" + channelId + ", title=" + title + ", link=" + link + ", linkMD5=" + linkMD5 + ", pubDate=" + pubDate + ", isRead=" + isRead + ", toSendEmail=" + toSendEmail + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((linkMD5 == null) ? 0 : linkMD5.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FeedItem other = (FeedItem) obj;
		if (linkMD5 == null) {
			if (other.linkMD5 != null)
				return false;
		} else if (!linkMD5.equals(other.linkMD5))
			return false;
		return true;
	}
}
