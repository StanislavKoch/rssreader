package com.hsd.rssreader.data.bean;

import java.util.Date;

public class Channel {

	private Integer id;
	private String title;
	private String userTitle;
	private String link;
	private String linkMD5;
	private Date lastBuildDate;
	private Date downloadDate;
	public Channel() {
		super();
	}
	public Channel(Integer id, String title, String userTitle, String link, String linkMD5, Date lastBuildDate, Date downloadDate) {
		super();
		this.id = id;
		this.title = title;
		this.userTitle = userTitle;
		this.link = link;
		this.linkMD5 = linkMD5;
		this.lastBuildDate = lastBuildDate;
		this.downloadDate = downloadDate;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUserTitle() {
		return userTitle;
	}
	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getLinkMD5() {
		return linkMD5;
	}
	public void setLinkMD5(String linkMD5) {
		this.linkMD5 = linkMD5;
	}
	public Date getLastBuildDate() {
		return lastBuildDate;
	}
	public void setLastBuildDate(Date lastBuildDate) {
		this.lastBuildDate = lastBuildDate;
	}
	public Date getDownloadDate() {
		return downloadDate;
	}
	public void setDownloadDate(Date downloadDate) {
		this.downloadDate = downloadDate;
	}
	
}
