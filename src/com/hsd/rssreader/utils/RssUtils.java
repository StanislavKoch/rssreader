package com.hsd.rssreader.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.hsd.rssreader.data.bean.FeedItem;
import com.hsd.rssreader.utils.exception.DownloadFailedException;

import android.util.Log;

public class RssUtils {

	private static DateFormat dateFormatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
	
	public static List<FeedItem> downloadRssFeed(Integer channelId, String link) throws DownloadFailedException {
		try {
			URL url = new URL(link);

			// Create a new HTTP URL connection
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConnection = (HttpURLConnection) connection;

			int responseCode = httpConnection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				InputStream in = httpConnection.getInputStream();
				return processRssStream(channelId, in);
			}
		} catch (MalformedURLException e) {
			Log.d("RssUtils", "Malformed URL Exception.", e);
		} catch (IOException e) {
			Log.d("RssUtils", "IO Exception.", e);
		}
		throw new DownloadFailedException();
	}

	private static List<FeedItem> processRssStream(Integer channelId, InputStream inputStream) {
		List<FeedItem> feeditems = new LinkedList<FeedItem>();
		// Create a new XML Pull Parser.
		XmlPullParserFactory factory;
		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			// Assign a new input stream.
			xpp.setInput(inputStream, null);
			int eventType = xpp.getEventType();
			// Continue until the end of the document is reached.  
			while (eventType != XmlPullParser.END_DOCUMENT) {
				// Check for a start tag of the results tag.
				if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("rss")) {
					eventType = xpp.next();
					// Process each result within the result tag.
					while (!(eventType == XmlPullParser.END_TAG && xpp.getName().equals("rss"))) {
						// Check for the name tag within the results tag.
						if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("channel")) {
							eventType = xpp.next();
							// Process each result within the result tag.
							while (!(eventType == XmlPullParser.END_TAG && xpp.getName().equals("channel"))) {
								// Check for the name tag within the results tag.
								if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("item")) {
									eventType = xpp.next();
									String title = null;
									String description = null;
									String link = null;
									Date pubDate = null;
									// Process each result within the result tag.
									while (!(eventType == XmlPullParser.END_TAG && xpp.getName().equals("item"))) {
										try {
											if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("title"))
												title = xpp.nextText();
											if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("link"))
												link = xpp.nextText();
											if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("description"))
												description = xpp.nextText();
											if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("pubDate"))
												pubDate = dateFormatter.parse(xpp.nextText());
										} catch (Exception ex) {
											ex.printStackTrace();
										}
										// Move on to the next tag.
										eventType = xpp.next();
									}
									if (title != null && link != null && description != null && pubDate != null) {
										feeditems.add(new FeedItem(channelId, title, link, PrimitiveUtils.md5(link), description, pubDate));
									}
								}
								// Move on to the next tag.
								eventType = xpp.next();
							}
							// Do something with each POI name.
						}
						// Move on to the next tag.
						eventType = xpp.next();
					}
					// Do something with each POI name.
				}
				// Move on to the next result tag.
				eventType = xpp.next();
			}
		} catch (XmlPullParserException e) {
			Log.d("RssUtils", "XML Pull Parser Exception", e);
		} catch (IOException e) {
			Log.d("RssUtils", "IO Exception", e);
		}
		return feeditems;
	}

}
