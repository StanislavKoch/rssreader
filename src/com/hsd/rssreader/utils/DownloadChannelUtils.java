package com.hsd.rssreader.utils;

import java.util.Date;
import java.util.List;

import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.FeedItem;
import com.hsd.rssreader.provider.DataProviderFacade;
import com.hsd.rssreader.receiver.DownloadReceiver;
import com.hsd.rssreader.service.DownloadChannelService;
import com.hsd.rssreader.utils.exception.DownloadFailedException;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

public class DownloadChannelUtils {

	public static void scheduleReloading(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		Integer refreshPeriod = prefs.getInt(Consts.PREF_REFRESH_PERIOD, Consts.PREF_REFRESH_PERIOD_DEFAULT);

		Intent alarmIntent = new Intent(context, DownloadReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);

		AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		mgr.cancel(pi);
		mgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + refreshPeriod, refreshPeriod, pi);
	}

	public static Boolean downloadChannel(Context context, Integer id, String link) {
		Log.w("DownloadChannelUtils", "downloadChannel()");
		Boolean isNewFeeditemsExists = false;

		List<FeedItem> newFeeditems; 
		try {
			newFeeditems = RssUtils.downloadRssFeed(id, link);
			DataProviderFacade.updateChannelTimestamp(context, id, new Date());
		} catch (DownloadFailedException e) {
			return false;
		} 
		List<FeedItem> storedFeeditems = DataProviderFacade.getChannelItems(context, id);
		for (FeedItem feeditem : newFeeditems) {
			Log.i("DownloadChannelUtils", "downloadChannelInfo() [feeditem:" + feeditem + "]");
		}
		Log.w("DownloadChannelUtils", "downloadChannel() [newFeeditems.size:" + newFeeditems.size() + "] [storedFeeditems.size:" + storedFeeditems.size() + "]");
		while (newFeeditems.removeAll(storedFeeditems));
		Log.w("DownloadChannelUtils", "downloadChannel() [newFeeditems.size:" + newFeeditems.size() + "]");
		if (!newFeeditems.isEmpty()) {
			DataProviderFacade.addFeeditems(context, newFeeditems);
			isNewFeeditemsExists = true;
		}
		
		return isNewFeeditemsExists;
	}
	
}
