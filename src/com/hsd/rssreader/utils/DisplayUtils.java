package com.hsd.rssreader.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;

public class DisplayUtils {

	public static Integer getDisplayWidth(Context context) {
		WindowManager window = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = window.getDefaultDisplay();
		return display.getWidth();
	}

	public static Integer getDisplayHeight(Context context) {
		WindowManager window = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = window.getDefaultDisplay();
		return display.getHeight();
	}

	public static Boolean isDisplayLarge(Context context) {
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE;
	}
	
	public static float getPxByMm(Activity context, Float mm) { 
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		Float xdpi = metrics.xdpi > 80 ? metrics.xdpi : 156;
		Float px = mm * xdpi * (1.0f/25.4f);
		return px;
	} 

}
