package com.hsd.rssreader.activity.feedpager;

import com.hsd.rssreader.R;
import com.hsd.rssreader.activity.ChannelsSDK11Activity;

import android.app.Activity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

public class PagerSDK11Builder implements PagerBuilder {

	public void build(FragmentActivity activity, final ViewPager channelPager, ChannelPagerAdapter channelPagerAdapter) {
		// Set up the action bar.  
		final ActionBar actionBar = ((ActionBarActivity) activity).getSupportActionBar(); 
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		// Set up the ViewPager with the sections adapter.
		channelPager.setAdapter(channelPagerAdapter); 

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		channelPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override   
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			} 
		}); 

		ActionBar.TabListener channelTabListener = new ActionBar.TabListener() { 
			@Override
			public void onTabReselected(Tab tab, FragmentTransaction fragmentTransaction) {
			}
			@Override
			public void onTabSelected(Tab tab, FragmentTransaction fragmentTransaction) {
				// When the given tab is selected, switch to the corresponding page in
				// the ViewPager. 
				channelPager.setCurrentItem(tab.getPosition());
			}
			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction fragmentTransaction) {
			}
		};
		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < channelPagerAdapter.getCount(); i++) {
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab().setText(channelPagerAdapter.getPageTitle(i)).setTabListener(channelTabListener));
		}
	}
	
	public void addTab(FragmentActivity activity, final ViewPager channelPager, String title) {
		final ActionBar actionBar = ((ActionBarActivity) activity).getSupportActionBar(); 
		ActionBar.TabListener channelTabListener = new ActionBar.TabListener() { 
			@Override
			public void onTabReselected(Tab tab, FragmentTransaction fragmentTransaction) {
			}
			@Override
			public void onTabSelected(Tab tab, FragmentTransaction fragmentTransaction) {
				// When the given tab is selected, switch to the corresponding page in
				// the ViewPager. 
				channelPager.setCurrentItem(tab.getPosition());
			}
			@Override
			public void onTabUnselected(Tab tab, FragmentTransaction fragmentTransaction) {
			}
		};

		actionBar.addTab(actionBar.newTab().setTabListener(channelTabListener).setText(title));
	}
	
}
