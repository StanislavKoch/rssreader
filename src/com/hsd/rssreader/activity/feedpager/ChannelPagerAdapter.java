package com.hsd.rssreader.activity.feedpager;

import java.io.ObjectInputStream.GetField;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import com.hsd.rssreader.R;
import com.hsd.rssreader.activity.tiles.ChannelActionListener;
import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.Channel;
import com.hsd.rssreader.data.bean.FeedItem;
import com.hsd.rssreader.provider.DataProviderFacade;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class ChannelPagerAdapter extends FragmentPagerAdapter {

	private FragmentManager fragmentManager;
	
	private Context context;
	
	private List<Channel> channels;
	private Map<Integer, ChannelFragment> fragments = new HashMap<Integer, ChannelFragment>();
	
	public ChannelPagerAdapter(Context context, FragmentManager fragmentManager) {
		super(fragmentManager);
		this.fragmentManager = fragmentManager;
		this.context = context;
		
		channels = DataProviderFacade.getChannels(context);
	}

	@Override
	public Fragment getItem(int position) {
		Log.w("ChannelPagerAdapter", "getItem() [position:" + position + "]");
		// getItem is called to instantiate the fragment for the given page.
		// Return a PlaceholderFragment (defined as a static inner class below).
		if (fragments.get(position) == null) { 
			fragments.put(position, ChannelFragment.newInstance(position + 1, channels.get(position).getId()));
		}
		return fragments.get(position);
	}

	@Override
	public int getItemPosition(Object object) {
		Log.w("ChannelPagerAdapter", "getItemPosition() [object:" + object + "]");
		return super.getItemPosition(object);
	}
	
	@Override
	public int getCount() {   
		return channels.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return channels.get(position).getUserTitle();
	}

	// *********************** interface **************************
	
	public ChannelFragment getItemSilent(Integer position) {
		return fragments.get(position);
	}
	
	public void putFragment(Integer position, ChannelFragment channelFragment) {
		fragments.put(position, channelFragment);
	}
	
	public void filter(String filter) {
		for (int i = 0; i < getCount(); i++) {
			ChannelFragment channelFragment = (ChannelFragment) getItem(i);
			Log.w("ChannelPagerAdapter", "filter() [isAdded:" + channelFragment.isAdded() + "] [isDetached:" + channelFragment.isDetached() + "] [isVisible:" + channelFragment.isVisible() + "] [isResumed:" + channelFragment.isResumed() + "]");
			if (channelFragment.isAdded() && !channelFragment.isDetached()) {
				channelFragment.updateUI(filter);
			}
		}
	}
	
	public void addChannel(Channel channel) {
		channels.add(channel);
	}
	
}

