package com.hsd.rssreader.activity.feedpager;

import java.util.Date;

import android.app.Activity;
import android.content.Context;
//import android.content.CursorLoader;
//import android.content.Loader;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.ResourceCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hsd.rssreader.R;
import com.hsd.rssreader.activity.tiles.ChannelActionListener;
import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.FeedItem;
import com.hsd.rssreader.provider.DataProvider;
import com.hsd.rssreader.provider.DataProviderFacade;
import com.hsd.rssreader.utils.DisplayUtils;
import com.hsd.rssreader.activity.tiles.ChannelsActivityListener;












//import android.app.LoaderManager; 
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Html;

/**
 * A placeholder fragment containing a simple view.
 */
public class ChannelFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
	private Handler handler;
	
	private LayoutInflater inflater;
	
	private ChannelsActivityListener channelsActivityListener;
	private ChannelActionListener channelActionListener;
	
	private Integer channelId;
	
	private CursorAdapter adapter;
	private Cursor feeditemCursor;
	private FeedItem selectedFeedItem;
	
	/**
	 * Returns a new instance of this fragment for the given section
	 * number.
	 */
	public static ChannelFragment newInstance(int sectionNumber, Integer channelId) {
		Log.w("ChannelFragment", "***newInstance()");
		ChannelFragment fragment = new ChannelFragment();
		fragment.setChannelId(channelId);
		return fragment;
	}

	public ChannelFragment() {
		Log.w("ChannelFragment", "ChannelFragmentinit()");
		handler = new Handler(); 
	}
	
	@Override
	public void onAttach(Activity activity) {
		Log.w("ChannelFragment", "onAttach()");
		super.onAttach(activity);
		
		channelsActivityListener = (ChannelsActivityListener) activity;
		channelActionListener = ((ChannelsActivityListener) activity).getChannelActionListener();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.w("ChannelFragment", "onCreate() [savedInstanceState:" + savedInstanceState + "]");
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.w("ChannelFragment", "onActivityCreated() [savedInstanceState:" + savedInstanceState + "]");
		super.onActivityCreated(savedInstanceState);
		// Create a new Adapter and bind it to the List View
		adapter = new ResourceCursorAdapter (getActivity(), R.layout.item_feeditem, null) {
			@Override
			public void bindView(View view, Context context, Cursor cursor) {
				Log.d("ChannelFragment", "bindView(), [cursor.count:" + cursor.getCount() + "]");
				Integer margin = (int) DisplayUtils.getPxByMm(getActivity(), 1.73f);
				LinearLayout itemOuterCont = (LinearLayout) view.findViewById(R.id.itemOuterCont);
				LinearLayout itemInnerCont = (LinearLayout) view.findViewById(R.id.itemInnerCont);
				LinearLayout emailIconCont = (LinearLayout) view.findViewById(R.id.emailIconCont);
				final ImageView emailIcon = (ImageView) view.findViewById(R.id.emailIcon);
				final TextView itemText = (TextView) view.findViewById(R.id.itemText);
				
				LinearLayout.LayoutParams lp;    
				lp= (LinearLayout.LayoutParams) itemInnerCont.getLayoutParams();
				lp.setMargins(margin, margin/3, 0, margin/3); 
				itemInnerCont.setLayoutParams(lp);  
				itemInnerCont.setPadding(margin, margin, margin, margin);
				lp= (LinearLayout.LayoutParams) emailIconCont.getLayoutParams();
				lp.setMargins(0, margin/3, margin, margin/3);    
				emailIconCont.setLayoutParams(lp);  
				emailIconCont.setPadding(margin, 0, 0, 0);
				lp= (LinearLayout.LayoutParams) emailIcon.getLayoutParams(); 
				lp.height = (int) DisplayUtils.getPxByMm(getActivity(), 4.6f); 
				lp.width = (int) DisplayUtils.getPxByMm(getActivity(), 4.6f); 
				emailIcon.setLayoutParams(lp);   

				final FeedItem feeditem = new FeedItem(cursor.getInt(Consts.COLUMN_INDEX_ID)
						, cursor.getInt(Consts.COLUMN_INDEX_FEEDITEM_CHANNEL_ID)
						, cursor.getString(Consts.COLUMN_INDEX_FEEDITEM_TITLE)
						, cursor.getString(Consts.COLUMN_INDEX_FEEDITEM_LINK)    
						, cursor.getString(Consts.COLUMN_INDEX_FEEDITEM_LINK_MD5)   
						, cursor.getString(Consts.COLUMN_INDEX_FEEDITEM_DESCRIPTION)
						, new Date(cursor.getLong(Consts.COLUMN_INDEX_FEEDITEM_PUB_DATE)) 
						, cursor.getInt(Consts.COLUMN_INDEX_FEEDITEM_IS_READ) == 1 
						, cursor.getInt(Consts.COLUMN_INDEX_FEEDITEM_TO_SEND_EMAIL) == 1
						); 
				
				itemText.setText(Html.fromHtml(feeditem.getTitle()));
				itemText.setTypeface(null, feeditem.getIsRead() ? Typeface.NORMAL : Typeface.BOLD);
				emailIcon.setBackgroundResource(feeditem.getToSendEmail() ? R.drawable.icon_email_active : R.drawable.icon_email_inactive);
				itemOuterCont.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						DataProviderFacade.updateMarkReadFeeditem(getActivity(), feeditem, true, feeditem.getToSendEmail());
						itemText.setTypeface(null, Typeface.NORMAL);
						selectedFeedItem = feeditem;
						channelActionListener.onItemClicked(selectedFeedItem);
					}
				});
				emailIconCont.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						feeditem.setToSendEmail(!feeditem.getToSendEmail());
						DataProviderFacade.updateMarkReadFeeditem(getActivity(), feeditem, feeditem.getIsRead(), feeditem.getToSendEmail());
						emailIcon.setBackgroundResource(feeditem.getToSendEmail() ? R.drawable.icon_email_active : R.drawable.icon_email_inactive);
					}
				});
			}}; 
		setListAdapter(adapter);

		Bundle data = new Bundle();
		data.putString(Consts.BUNDLE_FILTER, channelsActivityListener.getFilter());
		getLoaderManager().initLoader(android.R.id.list, data, this);
 
		Log.w("ChannelFragment", "onActivityCreated() EXIT");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.w("ChannelFragment", "onCreateView() [savedInstanceState:" + savedInstanceState + "]");
		this.inflater = inflater;
		View rootView = inflater.inflate(R.layout.fragment_channel, container, false);
  
		if (savedInstanceState != null) {
			selectedFeedItem = savedInstanceState.getParcelable(Consts.UI_SESSION_CHANNEL_FEED_ITEM);
			channelId = savedInstanceState.getInt(Consts.UI_SESSION_CHANNEL_CHANNEL_ID);
		}
		Log.w("ChannelFragment", "onCreateView() [channelId:" + channelId + "]");
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		Log.w("ChannelFragment", "onViewCreated() [savedInstanceState:" + savedInstanceState + "]");
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		Log.w("ChannelFragment", "onViewStateRestored() [savedInstanceState:" + savedInstanceState + "]");
		super.onViewStateRestored(savedInstanceState);
	}

	@Override
	public void onStart() {
		Log.w("ChannelFragment", "onStart()");
		super.onStart();

	}

	@Override
	public void onResume() {
		Log.w("ChannelFragment", "onResume() [channelId:" + channelId + "]");
		super.onResume();
	} 

	@Override
	public void onPause() {
		Log.w("ChannelFragment", "onPause()");
		super.onPause(); 
	}

	@Override
	public void onStop() {
		Log.w("ChannelFragment", "onStop");
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		Log.w("ChannelFragment", "onSaveInstanceState() [channelId:" + channelId + "]");
		super.onSaveInstanceState(outState);
		
		if (channelId != null) {  
			outState.putInt(Consts.UI_SESSION_CHANNEL_CHANNEL_ID, channelId);
		} 
		outState.putParcelable(Consts.UI_SESSION_CHANNEL_FEED_ITEM, selectedFeedItem);
//		}
	}

	@Override
	public void onDestroyView() { 
		Log.w("ChannelFragment", "onDestroyView()");
		super.onDestroyView(); 
	}

	@Override
	public void onDestroy() {
		Log.w("ChannelFragment", "onDestroy()"); 
		super.onDestroy();
	}

	@Override
	public void onDetach() { 
		Log.w("ChannelFragment", "onDetach()");
		super.onDetach();
	}

	// ******************* LoaderCallback ********************
	
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle data) {
		Log.w("ChannelFragment", "LoaderManager.LoaderCallbacks.onCreateLoader(), [BUNDLE_FILTER:" + (data != null ? data.get(Consts.BUNDLE_FILTER) : "ISNULL") + "], [channelId:" + channelId + "]");

		String selection;
		String[] selectionArgs;
		if (data != null && data.containsKey(Consts.BUNDLE_FILTER) && data.get(Consts.BUNDLE_FILTER) != null) {
			String filter = data.getString(Consts.BUNDLE_FILTER);
			selection = Consts.DB_COLUMN_FEEDITEM_CHANNEL_ID + "=? AND (upper(" + Consts.DB_COLUMN_FEEDITEM_TITLE 
					+ ") LIKE '%" + filter.toUpperCase() + "%' OR upper(" + Consts.DB_COLUMN_FEEDITEM_DESCRIPTION + ") LIKE '%" 
					+ filter.toUpperCase() + "%')";
			selectionArgs = new String[] { "" + channelId };
		} else {   
			selection = Consts.DB_COLUMN_FEEDITEM_CHANNEL_ID + "=?";
			selectionArgs = new String[] { "" + channelId };
		}
		CursorLoader loader = new CursorLoader(getActivity(), Consts.CONTENT_FEEDITEM_URI
				, DataProvider.sFeeditemProjectionMap.keySet().toArray(new String[DataProvider.sFeeditemProjectionMap.size()])
				, selection, selectionArgs, Consts.DB_COLUMN_FEEDITEM_PUB_DATE + " DESC");

		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		Log.w("ChannelFragment", "LoaderManager.LoaderCallbacks.onLoadFinished(), [count:" + cursor.getCount() + "]");
		adapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		Log.w("ChannelFragment", "LoaderManager.LoaderCallbacks.onLoaderReset()");
		adapter.swapCursor(null);
	}

	// *************** interface *******************
	
	public void updateUI(final String filter) {
		Log.w("ChannelFragment", "updateUI(), [filter:" + filter + "]");
		handler.post(new Runnable() {
			public void run() {
				try {
					Bundle data = new Bundle();
					data.putString(Consts.BUNDLE_FILTER, filter);
					getLoaderManager().restartLoader(0, data, ChannelFragment.this);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
	}

	public void setChannelId(Integer channelId) {
		this.channelId = channelId;
	}
	
	
}

