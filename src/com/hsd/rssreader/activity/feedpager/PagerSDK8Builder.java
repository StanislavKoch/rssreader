package com.hsd.rssreader.activity.feedpager;

import com.hsd.rssreader.R;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;

public class PagerSDK8Builder implements PagerBuilder {

	public void build(FragmentActivity activity, ViewPager channelPager, ChannelPagerAdapter channelPagerAdapter) {
		// Set up the ViewPager with the sections adapter.
		channelPager.setAdapter(channelPagerAdapter); 

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		channelPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override 
			public void onPageSelected(int position) {
//				actionBar.setSelectedNavigationItem(position);
			}
		});
	}

	@Override
	public void addTab(FragmentActivity activity, ViewPager channelPager, String title) {
	}
	
}
