package com.hsd.rssreader.activity.feedpager;

import android.os.Build;

public class PagerBuilderFactory {

	public static PagerBuilder getPagerBuilder() {
		return Build.VERSION.SDK_INT < 11 ? new PagerSDK8Builder() : new PagerSDK11Builder();
	}
	
}
