package com.hsd.rssreader.activity.feedpager;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;

public interface PagerBuilder {

	void build(FragmentActivity activity, ViewPager channelPager, ChannelPagerAdapter channelPagerAdapter);
	void addTab(FragmentActivity activity, final ViewPager channelPager, String title);	
}
