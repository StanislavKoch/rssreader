package com.hsd.rssreader.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.hsd.rssreader.R;
import com.hsd.rssreader.R.id;
import com.hsd.rssreader.R.layout;
import com.hsd.rssreader.R.menu;
import com.hsd.rssreader.activity.feedpager.ChannelPagerAdapter;
import com.hsd.rssreader.activity.feedpager.PagerBuilderFactory;
import com.hsd.rssreader.utils.DownloadChannelUtils;

public class ChannelsEntryActivity extends Activity /*implements ActionBar.TabListener*/ {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.act_channels);   

		if (Build.VERSION.SDK_INT < 11) {
			startActivity(new Intent(this, ChannelsSDK8Activity.class));
		} else {  
			startActivity(new Intent(this, ChannelsSDK11Activity.class));
		}
		
		DownloadChannelUtils.scheduleReloading(this);
	}

}
