package com.hsd.rssreader.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

import com.hsd.rssreader.R;
import com.hsd.rssreader.R.id;
import com.hsd.rssreader.R.layout;
import com.hsd.rssreader.R.menu;
import com.hsd.rssreader.activity.feeditem.FeedItemFragment;
import com.hsd.rssreader.activity.feedpager.ChannelPagerAdapter;
import com.hsd.rssreader.activity.feedpager.PagerBuilderFactory;
import com.hsd.rssreader.activity.tiles.ChannelsActivityListener;
import com.hsd.rssreader.activity.tiles.ChannelActionListener;
import com.hsd.rssreader.activity.tiles.FeedItemActionListener;
import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.FeedItem;

public class FeedItemSDK8Activity extends FragmentActivity /*implements ActionBar.TabListener*/{

	private FrameLayout itemFragmentCont;
	private FeedItemFragment itemFragment;
	
	private FeedItem feedItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.w("FeedItemSDK8Activity", "onCreate() [savedInstanceState:" + savedInstanceState + "]");
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.act_feeditem);
		
		itemFragmentCont = (FrameLayout) this.findViewById(R.id.itemFragmentCont);
		if (itemFragmentCont != null) {
			if (savedInstanceState == null) {
				FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
				itemFragment = new FeedItemFragment(); 
				ft.add(R.id.itemFragmentCont, itemFragment, Consts.FRAGMENT_FEED_ITEM_TAG);
				ft.commit();   
			} else {
				itemFragment = (FeedItemFragment) getSupportFragmentManager().getFragment(savedInstanceState, Consts.UI_SESSION_FEED_ITEM_TAG); 
				Log.w("FeedItemSDK11Activity", "onCreate() [itemFragment:" + itemFragment + "]");
			}
		}
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		Log.w("FeedItemSDK8Activity", "onRestoreInstanceState() [savedInstanceState:" + savedInstanceState + "]");
		super.onRestoreInstanceState(savedInstanceState);
		
		if (savedInstanceState != null) {
			feedItem = savedInstanceState.getParcelable(Consts.UI_SESSION_FEED_ITEM);
			if (itemFragmentCont != null) {
				itemFragmentCont.setVisibility(feedItem == null ? View.GONE : View.VISIBLE);
			}
		}
	}

	@Override
	public void onAttachFragment(Fragment fragment) {
		Log.w("FeedItemSDK8Activity", "onAttachFragment() [fragment:" + fragment + "]");
		super.onAttachFragment(fragment);
	}

	@Override
	protected void onStart() {
		Log.w("FeedItemSDK8Activity", "onStart()");
		super.onStart();

		Log.w("FeedItemSDK11Activity", "onStart() [INTENT_FEEDITEM:" + getIntent().getParcelableExtra(Consts.INTENT_ACTIVITY_FEEDITEM) + "]");
		if (getIntent().getParcelableExtra(Consts.INTENT_ACTIVITY_FEEDITEM) != null) {
			feedItem = (FeedItem) getIntent().getParcelableExtra(Consts.INTENT_ACTIVITY_FEEDITEM);
			itemFragment.handleFeedItem(feedItem);
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		Log.w("FeedItemSDK8Activity", "onNewIntent() [INTENT_FEEDITEM:" + intent.getParcelableExtra(Consts.INTENT_ACTIVITY_FEEDITEM) + "]");
		super.onNewIntent(intent);
		if (intent.getParcelableExtra(Consts.INTENT_ACTIVITY_FEEDITEM) != null) {
			feedItem = (FeedItem) getIntent().getParcelableExtra(Consts.INTENT_ACTIVITY_FEEDITEM);
			itemFragment.handleFeedItem(feedItem);
		}
	}

	@Override
	protected void onResume() {
		Log.w("FeedItemSDK8Activity", "onResume()");
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.w("FeedItemSDK8Activity", "onPause()");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.w("FeedItemSDK8Activity", "onStop()");
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		Log.w("FeedItemSDK8Activity", "onSaveInstanceState()");
		super.onSaveInstanceState(outState);
		
		outState.putParcelable(Consts.UI_SESSION_FEED_ITEM, feedItem);
		getSupportFragmentManager().putFragment(outState, Consts.UI_SESSION_FEED_ITEM_TAG, itemFragment);
	}

	@Override
	protected void onDestroy() {
		Log.w("FeedItemSDK8Activity", "onDestroy()");
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.channels, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		return super.onOptionsItemSelected(item);
	}

	// ********************* interface **************************
	
}
