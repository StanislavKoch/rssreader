package com.hsd.rssreader.activity;

import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hsd.rssreader.R;
import com.hsd.rssreader.R.id;
import com.hsd.rssreader.R.layout;
import com.hsd.rssreader.R.menu;
import com.hsd.rssreader.activity.feedpager.ChannelFragment;
import com.hsd.rssreader.activity.feedpager.ChannelPagerAdapter;
import com.hsd.rssreader.activity.feedpager.PagerBuilderFactory;
import com.hsd.rssreader.activity.tiles.ChannelsActivityListener;
import com.hsd.rssreader.activity.tiles.ChannelActionListener;
import com.hsd.rssreader.activity.tiles.ChannelsActivityUtils;
import com.hsd.rssreader.activity.tiles.FeedItemActionListener;
import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.FeedItem;
import com.hsd.rssreader.provider.DataProviderFacade;
import com.hsd.rssreader.service.DownloadChannelService;

public class ChannelsSDK8Activity extends FragmentActivity implements ChannelsActivityListener /*implements ActionBar.TabListener*/{

	private LayoutInflater inflater;

	private LinearLayout searchViewCont;
	private SearchView searchView;
	private AutoCompleteTextView editSearchView;
	private ViewPager channelPager;
	private FrameLayout itemFragmentCont;

	private ChannelPagerAdapter channelPagerAdapter;
	private BroadcastReceiver onReloadReceiver;

	private ChannelActionListener channelActionListener;
	private FeedItemActionListener feedItemActionListener;
	
	private SharedPreferences prefs;
	private SharedPreferences.Editor prefsEditor;

	private String filter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.w("ChannelsSDK8Activity", "onCreate() [savedInstanceState:" + savedInstanceState + "]");
		channelActionListener = new ChannelActionListener() {
			@Override
			public void onItemClicked(FeedItem feedItem) {
				Log.w("ChannelsSDK8Activity", "onItemClicked(), [feedItem:" + feedItem + "]");
				Intent intent = new Intent(ChannelsSDK8Activity.this, FeedItemSDK8Activity.class);
				intent.putExtra(Consts.INTENT_ACTIVITY_FEEDITEM, feedItem);
				startActivity(intent);
			}
			@Override
			public void onEmailClicked(FeedItem feedItem) {
				Log.w("ChannelsSDK8Activity", "onEmailClicked(), [feedItem:" + feedItem + "]");
			}
		};
		feedItemActionListener = new FeedItemActionListener() {
			@Override
			public void onNoFeedItem() {
				Log.w("ChannelsSDK8Activity", "onNoFeedItem()");
			} 
		};

		super.onCreate(savedInstanceState);
		inflater = LayoutInflater.from(this); 

		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefsEditor = prefs.edit();
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.act_channels);

		searchViewCont = (LinearLayout) this.findViewById(R.id.searchViewCont);
		channelPager = (ViewPager) this.findViewById(R.id.pager);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.  
		channelPagerAdapter = new ChannelPagerAdapter(this, getSupportFragmentManager());
		PagerBuilderFactory.getPagerBuilder().build(this, channelPager, channelPagerAdapter);
		
		searchView = new SearchView(this);
		editSearchView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
		editSearchView.setTextColor(getResources().getColor(R.color.white));
		searchView.setOnQueryTextListener(new OnQueryTextListener() {
			@Override 
			public boolean onQueryTextSubmit(String filter) { 
				Log.w("ChannelsSDK8Activity", "onQueryTextSubmit() [filter:" + filter + "], [this.filter:" + ChannelsSDK8Activity.this.filter + "]");
				ChannelsSDK8Activity.this.filter = filter;
				channelPagerAdapter.filter(filter);
				return false;
			}
			@Override
			public boolean onQueryTextChange(String filter) {
				Log.w("ChannelsSDK8Activity", "onQueryTextChange() [filter:" + filter + "], [this.filter:" + ChannelsSDK8Activity.this.filter + "]");
				ChannelsSDK8Activity.this.filter = filter;
				channelPagerAdapter.filter(filter);
				return false; 
			}
		});
		searchViewCont.addView(searchView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		
		Log.w("ChannelsSDK8Activity", "onCreate() EXIT");
	}

	@Override
	public void onAttachFragment(Fragment fragment) {
		Log.w("ChannelsSDK8Activity", "onAttachFragment() [fragment:" + fragment + "]");
		super.onAttachFragment(fragment);
	}

	@Override
	protected void onStart() {
		Log.w("ChannelsSDK8Activity", "onStart()");
		super.onStart();

		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Consts.RECEIVER_ACTION_RELOAD_UI);
		onReloadReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.w("ChannelsSDK8Activity", "onReceive() RECEIVER_ACTION_RELOAD_UI");
			}};
		registerReceiver(onReloadReceiver, intentFilter);
		
        Intent intent = new Intent(this, DownloadChannelService.class);
        this.startService(intent);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		Log.w("ChannelsSDK8Activity", "onRestoreInstanceState() [savedInstanceState:" + savedInstanceState + "]");
		super.onRestoreInstanceState(savedInstanceState);

		if (savedInstanceState != null) {
			filter = savedInstanceState.getString(Consts.UI_SESSION_CHANNEL_FILTER);
			Log.w("ChannelsSDK8Activity", "onRestoreInstanceState() [filter:" + filter + "]");
			for (int i = 0; i < channelPagerAdapter.getCount(); i++) {
				ChannelFragment channelFragment = (ChannelFragment) getSupportFragmentManager().getFragment(savedInstanceState, Consts.FRAGMENT_CHANNEL_TAG + i);
				channelPagerAdapter.putFragment(i, channelFragment);
			}

			// update UI 
			if (filter != null && filter.length() > 0) {
				editSearchView.setText(filter);
				searchView.setIconified(false);
			}   
			channelPagerAdapter.filter(filter);
		}  
	}

	@Override  
	protected void onNewIntent(Intent intent) {
		Log.w("ChannelsSDK8Activity", "onNewIntent() [intent:" + intent + "]");
		super.onNewIntent(intent);
	}

	@Override
	protected void onResume() {
		Log.w("ChannelsSDK8Activity", "onResume()");
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.w("ChannelsSDK8Activity", "onPause()");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.w("ChannelsSDK8Activity", "onStop()");
		super.onStop();

		unregisterReceiver(onReloadReceiver);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		Log.w("ChannelsSDK8Activity", "onSaveInstanceState()");
		super.onSaveInstanceState(outState);

		outState.putString(Consts.UI_SESSION_CHANNEL_FILTER, filter);
		for (int i = 0; i < channelPagerAdapter.getCount(); i++) { 
			ChannelFragment channelFragment = channelPagerAdapter.getItemSilent(i);
			Log.w("ChannelPagerAdapter", "onSaveInstanceState() [isAdded:" + (channelFragment != null ? channelFragment.isAdded() : "ISNULL") + "] [isDetached:" + (channelFragment != null ? channelFragment.isDetached() : "ISNULL") + "] [isVisible:" + (channelFragment != null ? channelFragment.isVisible() : "ISNULL") + "] [isResumed:" + (channelFragment != null ? channelFragment.isResumed() : "ISNULL") + "]");
			if (channelFragment != null && (channelFragment.isAdded() || channelFragment.isDetached())) { 
				Log.w("ChannelsSDK11Activity", "onSaveInstanceState() [i:" + i + "]");
				getSupportFragmentManager().putFragment(outState, Consts.FRAGMENT_CHANNEL_TAG + i, channelPagerAdapter.getItemSilent(i));
			}
		}
	} 

	@Override
	protected void onDestroy() {
		Log.w("ChannelsSDK8Activity", "onDestroy()");
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.w("ChannelsSDK8Activity", "onCreateOptionsMenu()");
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.channels, menu);
		menu.removeItem(R.id.action_search);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.w("ChannelsSDK8Activity", "onOptionsItemSelected()");
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_refresh) {
            Intent intent = new Intent(this, DownloadChannelService.class);
            intent.putExtra(Consts.INTENT_DOWNLOAD_FORCE, true);
            this.startService(intent);
			return true;
		} 
		if (id == R.id.action_mark_read) {
			DataProviderFacade.updateMarkReadAllFeeditem(this);
			return true;
		}
		if (id == R.id.action_send_email) {
			List<FeedItem> feeditems = DataProviderFacade.getChannelItemsForEmail(this);
			ChannelsActivityUtils.sendEmail(this, feeditems);
			DataProviderFacade.updateMarkSentAllFeeditem(this);
			return true;
		}
		if (id == R.id.action_add_channel) {
			ChannelsActivityUtils.showAddChannelDialog(this, inflater, channelPagerAdapter, channelPager);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// ********************* interface **************************
	
	@Override
	public ChannelActionListener getChannelActionListener() {
		return channelActionListener;
	}

	@Override
	public FeedItemActionListener getFeedItemActionListener() {
		return feedItemActionListener;
	}

	public String getFilter() {
		return filter;
	}
	
}
