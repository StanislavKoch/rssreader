package com.hsd.rssreader.activity.tiles;

import com.hsd.rssreader.data.bean.FeedItem;

public interface ChannelActionListener {

	void onItemClicked(FeedItem feedItem);
	void onEmailClicked(FeedItem feedItem);
	
}
