package com.hsd.rssreader.activity.tiles;

import com.hsd.rssreader.data.bean.FeedItem;

public interface FeedItemActionListener {

	void onNoFeedItem();
	
}
