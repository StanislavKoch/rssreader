package com.hsd.rssreader.activity.tiles;

public interface ChannelsActivityListener {

	ChannelActionListener getChannelActionListener();
	FeedItemActionListener getFeedItemActionListener();
	
	String getFilter();
	
}
