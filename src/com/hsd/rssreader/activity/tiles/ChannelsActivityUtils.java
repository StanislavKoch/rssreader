package com.hsd.rssreader.activity.tiles;

import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.hsd.rssreader.R;
import com.hsd.rssreader.activity.feedpager.ChannelPagerAdapter;
import com.hsd.rssreader.activity.feedpager.PagerBuilderFactory;
import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.Channel;
import com.hsd.rssreader.data.bean.FeedItem;
import com.hsd.rssreader.provider.DataProviderFacade;
import com.hsd.rssreader.service.DownloadChannelService;

public class ChannelsActivityUtils {

	// *********************** show *********************
	
	public static void showAddChannelDialog(final FragmentActivity activity, LayoutInflater inflater, final ChannelPagerAdapter channelPagerAdapter, final ViewPager channelPager) {
		View view = inflater.inflate(R.layout.dialog_channel_add, null);
		final EditText editTitle = (EditText) view.findViewById(R.id.editTitle);
		final EditText editLink = (EditText) view.findViewById(R.id.editLink);
		new AlertDialog.Builder(activity) 
	    .setTitle(R.string.dialog_channel_add_new_channel)
	    .setView(view)
	    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	        	try {
		            String title = editTitle.getText().toString(); 
		            String link = editLink.getText().toString(); 
		            DataProviderFacade.addChannel(activity, title, title, link);
		            Channel channel = DataProviderFacade.getChannelByLink(activity, link);
		            
		            channelPagerAdapter.addChannel(channel);
		            PagerBuilderFactory.getPagerBuilder().addTab(activity, channelPager, channel.getUserTitle());
		            channelPagerAdapter.notifyDataSetChanged();
		            
		            Intent intent = new Intent(activity, DownloadChannelService.class);
		            intent.putExtra(Consts.INTENT_DOWNLOAD_CHANNEL_ID, channel.getId());
		            intent.putExtra(Consts.INTENT_DOWNLOAD_FORCE, true);
		            activity.startService(intent);
	        	} catch (SQLException ex) {
	        		Toast.makeText(activity, "This RSS is already exists", Toast.LENGTH_LONG).show();
	        	}
	        }
	    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	            // Do nothing.
	        }
	    }).show();
	}
	
	public static void sendEmail(Context context, List<FeedItem> feeditems) {
		StringBuffer sb = new StringBuffer();
		for (FeedItem feeditem : feeditems) {
			sb.append(feeditem.getLink() + "\r\n");
		}
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"testrssreader@mailinator.com"});
		i.putExtra(Intent.EXTRA_SUBJECT, "RSSReader, interesting info");
		i.putExtra(Intent.EXTRA_TEXT   , sb.toString());
		try {
			context.startActivity(Intent.createChooser(i, "Send mail..."));
		} catch (android.content.ActivityNotFoundException ex) {
		    Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
		}
	}
	
}
