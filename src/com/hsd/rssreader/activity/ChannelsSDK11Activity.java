package com.hsd.rssreader.activity;

import java.util.List;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
//import android.widget.SearchView; 





















import android.widget.Toast;

import com.hsd.rssreader.R;
import com.hsd.rssreader.R.id;
import com.hsd.rssreader.R.layout;
import com.hsd.rssreader.R.menu;
import com.hsd.rssreader.activity.feeditem.FeedItemFragment;
import com.hsd.rssreader.activity.feedpager.ChannelFragment;
import com.hsd.rssreader.activity.feedpager.ChannelPagerAdapter;
import com.hsd.rssreader.activity.feedpager.PagerBuilderFactory;
import com.hsd.rssreader.activity.tiles.ChannelsActivityListener;
import com.hsd.rssreader.activity.tiles.ChannelActionListener;
import com.hsd.rssreader.activity.tiles.ChannelsActivityUtils;
import com.hsd.rssreader.activity.tiles.FeedItemActionListener;
import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.Channel;
import com.hsd.rssreader.data.bean.FeedItem;
import com.hsd.rssreader.provider.DataProviderFacade;
import com.hsd.rssreader.service.DownloadChannelService;

public class ChannelsSDK11Activity extends ActionBarActivity implements ChannelsActivityListener  {

	private LayoutInflater inflater;

	private SearchView searchView;
	private AutoCompleteTextView editSearchView;
	private ViewPager channelPager;
	private FrameLayout itemFragmentCont;
	private FeedItemFragment itemFragment;
	
	private ChannelPagerAdapter channelPagerAdapter;
	private BroadcastReceiver onReloadReceiver;

	private ChannelActionListener channelActionListener;
	private FeedItemActionListener feedItemActionListener;

	private String filter;
	private FeedItem feedItem;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.w("ChannelsSDK11Activity", "onCreate() [savedInstanceState:" + savedInstanceState + "]");
		channelActionListener = new ChannelActionListener() {
			@Override
			public void onItemClicked(FeedItem feedItem) {
				Log.w("ChannelsSDK11Activity", "onItemClicked(), [feedItem:" + feedItem + "]");
				ChannelsSDK11Activity.this.feedItem = feedItem;
				if (itemFragmentCont != null) {
					itemFragmentCont.setVisibility(View.VISIBLE);
					itemFragment.handleFeedItem(feedItem); 
				} else {
					Intent intent = new Intent(ChannelsSDK11Activity.this, FeedItemSDK11Activity.class);
					intent.putExtra(Consts.INTENT_ACTIVITY_FEEDITEM, feedItem);
					startActivity(intent);
				}
			}
			@Override
			public void onEmailClicked(FeedItem feedItem) {
				Log.w("ChannelsSDK11Activity", "onEmailClicked(), [feedItem:" + feedItem + "]");
			} 
		};
		feedItemActionListener = new FeedItemActionListener() {
			@Override
			public void onNoFeedItem() {
				Log.w("ChannelsSDK11Activity", "onNoFeedItem()");
				ChannelsSDK11Activity.this.feedItem = null;
				if (itemFragmentCont != null) {
					itemFragmentCont.setVisibility(View.GONE);
				} 
			}
		};

		super.onCreate(savedInstanceState);
		inflater = LayoutInflater.from(this); 

		setContentView(R.layout.act_channels);   

		channelPager = (ViewPager) this.findViewById(R.id.pager);
		itemFragmentCont = (FrameLayout) this.findViewById(R.id.itemFragmentCont);
		if (itemFragmentCont != null) {
			if (savedInstanceState == null) {
				FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
				itemFragment = new FeedItemFragment(); 
				ft.add(R.id.itemFragmentCont, itemFragment, "itemFragmentTag");
				ft.commit();
			} else {
				itemFragment = (FeedItemFragment) getSupportFragmentManager().getFragment(savedInstanceState, Consts.UI_SESSION_FEED_ITEM_TAG); 
				Log.w("ChannelsSDK11Activity", "onCreate() [itemFragment:" + itemFragment + "]");
			}
		} 
		
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.  
		channelPagerAdapter = new ChannelPagerAdapter(this, getSupportFragmentManager());  
		PagerBuilderFactory.getPagerBuilder().build(this, channelPager, channelPagerAdapter);
		Log.w("ChannelsSDK11Activity", "onCreate() EXIT");
	}

	@Override
	public void onAttachFragment(Fragment fragment) { 
		Log.w("ChannelsSDK11Activity", "onAttachFragment() [fragment:" + fragment + "]");
		super.onAttachFragment(fragment);
	}

	@Override
	protected void onStart() {
		Log.w("ChannelsSDK11Activity", "onStart()");
		super.onStart(); 
		
		IntentFilter intentFilter = new IntentFilter(); 
		intentFilter.addAction(Consts.RECEIVER_ACTION_RELOAD_UI);
		onReloadReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.w("ChannelsSDK11Activity", "onReceive() RECEIVER_ACTION_RELOAD_UI");
			}};
		registerReceiver(onReloadReceiver, intentFilter);
	
        Intent intent = new Intent(this, DownloadChannelService.class);
        this.startService(intent);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		Log.w("ChannelsSDK11Activity", "onRestoreInstanceState() [savedInstanceState:" + savedInstanceState + "]");
		super.onRestoreInstanceState(savedInstanceState);
		
		if (savedInstanceState != null) {
			filter = savedInstanceState.getString(Consts.UI_SESSION_CHANNEL_FILTER);
			feedItem = savedInstanceState.getParcelable(Consts.UI_SESSION_CHANNEL_FEED_ITEM);
			if (itemFragmentCont != null) {
				itemFragmentCont.setVisibility(feedItem == null ? View.GONE : View.VISIBLE);
			}
			for (int i = 0; i < channelPagerAdapter.getCount(); i++) {
				ChannelFragment channelFragment = (ChannelFragment) getSupportFragmentManager().getFragment(savedInstanceState, Consts.FRAGMENT_CHANNEL_TAG + i);
				channelPagerAdapter.putFragment(i, channelFragment);
			}
		} 
	}

	@Override
	protected void onNewIntent(Intent intent) {
		Log.w("ChannelsSDK11Activity", "onNewIntent() [intent:" + intent + "]");
		super.onNewIntent(intent);
	}

	@Override
	protected void onResume() {
		Log.w("ChannelsSDK11Activity", "onResume()");
		super.onResume();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Log.w("ChannelsSDK11Activity", "KEYCODE_BACK()");
			if (itemFragmentCont.getVisibility() == View.VISIBLE) {
				itemFragmentCont.setVisibility(View.GONE);
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onPause() {
		Log.w("ChannelsSDK11Activity", "onPause()");
		super.onPause();
	}

	@Override
	protected void onStop() {
		Log.w("ChannelsSDK11Activity", "onStop()");
		super.onStop();

		unregisterReceiver(onReloadReceiver);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		Log.w("ChannelsSDK11Activity", "onSaveInstanceState()");
		super.onSaveInstanceState(outState);
		
		outState.putParcelable(Consts.UI_SESSION_CHANNEL_FEED_ITEM, feedItem);
		outState.putString(Consts.UI_SESSION_CHANNEL_FILTER, filter);
		if (itemFragment != null) {
			getSupportFragmentManager().putFragment(outState, Consts.UI_SESSION_FEED_ITEM_TAG, itemFragment);
		}
		for (int i = 0; i < channelPagerAdapter.getCount(); i++) {
			ChannelFragment channelFragment = channelPagerAdapter.getItemSilent(i);
			if (channelFragment != null && (channelFragment.isAdded() || channelFragment.isDetached())) { 
				Log.w("ChannelsSDK11Activity", "onSaveInstanceState() [i:" + i + "]");
				Log.w("ChannelPagerAdapter", "onSaveInstanceState() [isAdded:" + channelFragment.isAdded() + "] [isDetached:" + channelFragment.isDetached() + "] [isVisible:" + channelFragment.isVisible() + "] [isResumed:" + channelFragment.isResumed() + "]");
				getSupportFragmentManager().putFragment(outState, Consts.FRAGMENT_CHANNEL_TAG + i, channelPagerAdapter.getItemSilent(i));
			}
		}
	}

	@Override
	protected void onDestroy() {
		Log.w("ChannelsSDK11Activity", "onDestroy()");
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.w("ChannelsSDK11Activity", "onCreateOptionsMenu()");
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.channels, menu);

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		MenuItem searchItem = menu.findItem(R.id.action_search);
		searchView = new SearchView(this);
		editSearchView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
		editSearchView.setTextColor(getResources().getColor(R.color.white));
		searchView.setOnQueryTextListener(new OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String filter) {
				Log.w("ChannelsSDK11Activity", "onQueryTextSubmit() [filter:" + filter + "], [this.filter:" + ChannelsSDK11Activity.this.filter + "]");
				ChannelsSDK11Activity.this.filter = filter;
				channelPagerAdapter.filter(filter);
				return false;
			}
			@Override
			public boolean onQueryTextChange(String filter) {
				Log.w("ChannelsSDK11Activity", "onQueryTextChange() [filter:" + filter + "], [this.filter:" + ChannelsSDK11Activity.this.filter + "]");
				ChannelsSDK11Activity.this.filter = filter;
				channelPagerAdapter.filter(filter);
				return false;
			}
		});
		
		if (filter != null && filter.length() > 0) {
			editSearchView.setText(filter);
			searchView.setIconified(false);
		}
		for (View child : searchView.getTouchables()) {
			Log.w("ChannelsSDK11Activity", "onCreateOptionsMenu(), getTouchables() [child:" + child.getClass().getCanonicalName() + "]");
		}
		
		MenuItemCompat.setShowAsAction(searchItem, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
		MenuItemCompat.setActionView(searchItem, searchView);
		
		return true; 
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.w("ChannelsSDK11Activity", "onOptionsItemSelected()");
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_search) {
		    // Moved from onCreate -- Retreive the Search View and configure/enable it.
		    return true;
		}
		if (id == R.id.action_refresh) {
            Intent intent = new Intent(this, DownloadChannelService.class);
            intent.putExtra(Consts.INTENT_DOWNLOAD_FORCE, true);
            this.startService(intent);
			return true;
		}
		if (id == R.id.action_mark_read) {
			DataProviderFacade.updateMarkReadAllFeeditem(this);
			return true;
		}
		if (id == R.id.action_send_email) {
			List<FeedItem> feeditems = DataProviderFacade.getChannelItemsForEmail(this);
			ChannelsActivityUtils.sendEmail(this, feeditems);
			DataProviderFacade.updateMarkSentAllFeeditem(this);
			return true;
		}
		if (id == R.id.action_add_channel) {
			ChannelsActivityUtils.showAddChannelDialog(this, inflater, channelPagerAdapter, channelPager);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// ********************* interface **************************
	
	@Override
	public ChannelActionListener getChannelActionListener() {
		return channelActionListener;
	}
	
	@Override
	public FeedItemActionListener getFeedItemActionListener() {
		return feedItemActionListener;
	}
	
	public String getFilter() {
		return filter;
	}

}
