package com.hsd.rssreader.activity.feeditem;

import java.text.DateFormat;
import java.util.Locale;

import com.hsd.rssreader.R;
import com.hsd.rssreader.activity.tiles.ChannelsActivityListener;
import com.hsd.rssreader.activity.tiles.FeedItemActionListener;
import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.FeedItem;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
 
public class FeedItemFragment extends Fragment {

	private static DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM, Locale.getDefault());
	
	private TextView itemTitle;
	private TextView itemPubDate;
	private TextView itemDescription;
	
	private FeedItem feedItem;
	
	@Override
	public void onAttach(Activity activity) {
		Log.w("FeedItemFragment", "onAttach()");
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.w("FeedItemFragment", "onCreate() [savedInstanceState:" + savedInstanceState + "]");
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.w("FeedItemFragment", "onActivityCreated() [savedInstanceState:" + savedInstanceState + "]");
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.w("FeedItemFragment", "onCreateView() [savedInstanceState:" + savedInstanceState + "]");
//		return super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_feeditem, container, false);
		
		itemTitle = (TextView) view.findViewById(R.id.itemTitle);
		itemPubDate = (TextView) view.findViewById(R.id.itemPubDate);
		itemDescription = (TextView) view.findViewById(R.id.itemDescription);
		
		if (savedInstanceState != null) {
			feedItem = savedInstanceState.getParcelable(Consts.UI_SESSION_CHANNEL_FEED_ITEM);
		}
		
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		Log.w("FeedItemFragment", "onViewCreated() [savedInstanceState:" + savedInstanceState + "]");
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState) {
		Log.w("FeedItemFragment", "onViewStateRestored() [savedInstanceState:" + savedInstanceState + "]");
		super.onViewStateRestored(savedInstanceState);
	}

	@Override
	public void onStart() {
		Log.w("FeedItemFragment", "onStart()");
		super.onStart();

		showFeedItem();
	}

	@Override
	public void onResume() {
		Log.w("FeedItemFragment", "onResume()");
		super.onResume();
	}

	@Override
	public void onPause() {
		Log.w("FeedItemFragment", "onPause()");
		super.onPause();
	}

	@Override
	public void onStop() {
		Log.w("FeedItemFragment", "onStop");
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		Log.w("FeedItemFragment", "onSaveInstanceState()");
		super.onSaveInstanceState(outState);
		
		outState.putParcelable(Consts.UI_SESSION_CHANNEL_FEED_ITEM, feedItem);
	}

	@Override
	public void onDestroyView() {
		Log.w("FeedItemFragment", "onDestroyView()");
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		Log.w("FeedItemFragment", "onDestroy()");
		super.onDestroy();
	}

	@Override
	public void onDetach() {
		Log.w("FeedItemFragment", "onDetach()");
		super.onDetach();
	}

	// ************************ interface ********************
	
	public void handleFeedItem(FeedItem feedItem) {
		this.feedItem = feedItem;
		showFeedItem();
	}
	
	// ******************** show ******************
	
	private void showFeedItem() {
		Log.w("FeedItemFragment", "showFeedItem() [feedItem:" + feedItem + "]");
		itemTitle.setText(feedItem != null ? Html.fromHtml(feedItem.getTitle()) : ""); 
		itemPubDate.setText(feedItem != null ? dateFormat.format(feedItem.getPubDate()) : "");
		itemDescription.setText(feedItem != null ? Html.fromHtml(feedItem.getDescription()) : "");
		
		itemTitle.setSelected(true);
	}
	
}
