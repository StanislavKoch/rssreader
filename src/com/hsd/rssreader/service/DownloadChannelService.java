package com.hsd.rssreader.service;

import java.util.Date;
import java.util.List;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.Channel;
import com.hsd.rssreader.data.bean.FeedItem;
import com.hsd.rssreader.provider.DataProviderFacade;
import com.hsd.rssreader.utils.DownloadChannelUtils;
import com.hsd.rssreader.utils.RssUtils;

public class DownloadChannelService extends Service {
	
	@Override
	public void onCreate() { 
		super.onCreate();
		Log.w("DownloadChannelService", "onCreate");
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	 
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		Log.w("DownloadChannelService", "onStart, " + (intent == null ? "INTENT_NULL" : intent.getDataString()));
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		final Integer channelId = intent != null && intent.hasExtra(Consts.INTENT_DOWNLOAD_CHANNEL_ID) ? intent.getIntExtra(Consts.INTENT_DOWNLOAD_CHANNEL_ID, -1) : -1;
		final Boolean force = intent != null && intent.hasExtra(Consts.INTENT_DOWNLOAD_FORCE) ? intent.getBooleanExtra(Consts.INTENT_DOWNLOAD_FORCE, false) : false;
		Log.w("DownloadChannelService", "onStartCommand, [channelId:" + (intent == null ? "ISNULL" : channelId) + "]");
		new AsyncTask<Void, Void, Boolean>() {
			@Override
			protected Boolean doInBackground(Void... params) {
				Log.w("DownloadChannelService", "onStartCommand, doInBackground()");
				Boolean isNewFeeditemsExists = false;
				
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(DownloadChannelService.this);
				Integer refreshPeriod = prefs.getInt(Consts.PREF_REFRESH_PERIOD, Consts.PREF_REFRESH_PERIOD_DEFAULT);

				Cursor channelsCursor;
				if (channelId != -1) {
					// Download signle channel 
					Channel channel = DataProviderFacade.getChannelById(DownloadChannelService.this, channelId);
					Integer timeSinceLastDownload = (int) (new Date().getTime() - (channel.getDownloadDate() == null ? 0 : channel.getDownloadDate().getTime())) + 30 * 1000;
					if (force || timeSinceLastDownload > refreshPeriod) {
						isNewFeeditemsExists = DownloadChannelUtils.downloadChannel(DownloadChannelService.this, channel.getId(), channel.getLink());
					}
				} else {
					// Download all channels 
					channelsCursor = DataProviderFacade.getChannelsCursor(DownloadChannelService.this);
					if (channelsCursor.getCount() > 0) {
						try {
							while (channelsCursor.moveToNext()) { 
								Integer id = channelsCursor.getInt(Consts.COLUMN_INDEX_ID); 
								String link = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_LINK);
								Long downloadDate = channelsCursor.getLong(Consts.COLUMN_INDEX_CHANNEL_DOWNLOAD_DATE);
								Long timeSinceLastDownload = (new Date().getTime() - (downloadDate == null ? 0 : downloadDate)) + 30 * 1000;
								Log.w("DownloadChannelService", "onStartCommand, [id:" + id + "], [force:" + force + "], [timeSinceLastDownload:" + timeSinceLastDownload + "], [refreshPeriod:" + refreshPeriod + "]");
								if (force || timeSinceLastDownload > refreshPeriod) { 
									isNewFeeditemsExists = DownloadChannelUtils.downloadChannel(DownloadChannelService.this, id, link) || isNewFeeditemsExists;
								}
							}
						} finally {
							channelsCursor.close();
						}
					} else {
						channelsCursor.close();
					}
				}
				
				return isNewFeeditemsExists;
			}
			@Override
			protected void onPostExecute(Boolean isNewFeeditemsExists) {
				Log.w("DownloadChannelService", "onStartCommand, onPostExecute()");
				super.onPostExecute(isNewFeeditemsExists);
				if (isNewFeeditemsExists) {
					Intent intent = new Intent(Consts.RECEIVER_ACTION_RELOAD_UI);
					DownloadChannelService.this.sendBroadcast(intent);
				}
			}
		}.execute();
		
		return Service.START_NOT_STICKY;
	}

	public void onDestroy() {
		super.onDestroy();
		Log.w("DownloadChannelService", "onDestroy");
	}
 
}
