package com.hsd.rssreader.receiver;

import java.util.Date;
import java.util.List;

import com.hsd.rssreader.activity.ChannelsSDK11Activity;
import com.hsd.rssreader.consts.Consts;
import com.hsd.rssreader.data.bean.FeedItem;
import com.hsd.rssreader.provider.DataProviderFacade;
import com.hsd.rssreader.service.DownloadChannelService;
import com.hsd.rssreader.utils.DownloadChannelUtils;
import com.hsd.rssreader.utils.RssUtils;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

public class DownloadReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(final Context context, Intent intent) {
		Log.w("DownloadReceiver", "onReceive()");
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				// delete old feed items
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
				Long lastClearingDate = prefs.getLong(Consts.PREF_LAST_CLEARING_DATE, 0);
				if (new Date().getTime() - lastClearingDate > Consts.PREF_LAST_CLEARING_DATE_PERIOD) {
					DataProviderFacade.deleteOldFeeditems(context, new Date().getTime() - Consts.CLEARING_DATE_PERIOD);
					SharedPreferences.Editor prefsEditor = prefs.edit();
					prefsEditor.putLong(Consts.PREF_LAST_CLEARING_DATE, new Date().getTime());
					prefsEditor.apply();
				}
				
				// download new feed items
				Boolean isNewFeeditemsExists = false; 
				
				Cursor channelsCursor = DataProviderFacade.getChannelsCursor(context);
				if (channelsCursor.getCount() > 0) {
					try {
						while (channelsCursor.moveToNext()) {
							Integer id = channelsCursor.getInt(Consts.COLUMN_INDEX_ID);
							String link = channelsCursor.getString(Consts.COLUMN_INDEX_CHANNEL_LINK);

							isNewFeeditemsExists = isNewFeeditemsExists || DownloadChannelUtils.downloadChannel(context, id, link);
						}
					} finally {
						channelsCursor.close();
					}
				} else {
					channelsCursor.close();
				}
				
				if (isNewFeeditemsExists) {
					Intent intent = new Intent(Consts.RECEIVER_ACTION_RELOAD_UI);
					context.sendBroadcast(intent);
				}
				
				return null;
			}
		}.execute();
	}

}
