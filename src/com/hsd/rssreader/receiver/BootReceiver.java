package com.hsd.rssreader.receiver;

import com.hsd.rssreader.utils.DownloadChannelUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		DownloadChannelUtils.scheduleReloading(context);
	}

}
